import { PhotoDetailComponent } from './photos/photo-detail/photo-detail.component';
import { AuthGuardService } from './core/auth/auth-guard.service';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PhotoListComponent } from "./photos/photo-list/photo-list.component";
import { PhotoFormComponent } from "./photos/photo-form/photo-form.component";
import { NotFoundComponent } from "./errors/not-found/not-found.component";
import { PhotoListResolver } from "./photos/photo-list/photo-list.resolver";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule'
  },
  {
    path: "user/:userName",
    component: PhotoListComponent,
    resolve: {
      photos: PhotoListResolver,
    },
  },
  { 
    path: "p/add", 
    component: PhotoFormComponent,
    canActivate: [AuthGuardService] 
  },
  { 
    path: "p/:photoId", 
    component: PhotoDetailComponent
  },
  { 
    path: 'not-found',
    component: NotFoundComponent
},
{
    path: '**',
    redirectTo: 'not-found'
}
];

@NgModule({
/* Se precisar ativar o hash para navegadores que nao suportam history API:
  RouterModule.forRoot(routes, { useHash: true } )*/
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
