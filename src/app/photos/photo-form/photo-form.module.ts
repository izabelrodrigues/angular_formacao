import { ImmediateClickModule } from './../../shared/directives/immediate-click/immediate-click.module';
import { ImmediateClickDirective } from './../../shared/directives/immediate-click/immediate-click.directive';
import { PhotoModule } from './../photo/photo.module';
import { RouterModule } from '@angular/router';
import { VmessageModule } from './../../shared/components/vmessage/vmessage.module';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PhotoFormComponent } from './photo-form.component';

@NgModule({
    declarations: [ 
        PhotoFormComponent
    ],
    imports: [ 
        CommonModule, 
        ReactiveFormsModule, 
        VmessageModule, 
        RouterModule, 
        PhotoModule, 
        ImmediateClickModule 
    ]
})
export class PhotoFormModule {}
