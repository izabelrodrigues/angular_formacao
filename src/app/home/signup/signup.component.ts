import { SignupService } from "./signup.service";
import { UserNotTakenValidatorService } from "./user-not-taken-validator.service";
import { Validators } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { lowerCaseValidator } from "src/app/shared/validators/lowercase.validator";
import { UserForm } from "./user-form";
import { Router } from "@angular/router";
import { PlatformDetectorService } from 'src/app/core/platform-detector/platform-detector.service';

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"],
  providers: [UserNotTakenValidatorService]
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;

  @ViewChild("emailInput") userNameInput: ElementRef<HTMLInputElement>;

  constructor(
    private formBuilder: FormBuilder,
    private userNotTakenValidatorService: UserNotTakenValidatorService,
    private signupService: SignupService,
    private router: Router,
    private platformDetectorService: PlatformDetectorService
  ) {}

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      fullName: [
        "",
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(120),
        ],
      ],
      userName: [
        "",
        [
          Validators.required,
          lowerCaseValidator,
          Validators.minLength(2),
          Validators.maxLength(30),
        ],
        this.userNotTakenValidatorService.checkUserNameTaken(),
      ],
      password: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(14),
        ],
      ],
    });

    this.platformDetectorService.isPlatformBrowser() &&
    this.userNameInput.nativeElement.focus();
  }

  signUp() {
    const userForm = this.signupForm.getRawValue() as UserForm;
    this.signupService.signUp(userForm).subscribe(
      () => this.router.navigate([""]),
      (error) => console.log(error)
    );
  }
}
