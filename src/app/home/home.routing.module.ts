import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from './home.component';
import { LoginGuardService } from '../core/auth/login-guard.service';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [LoginGuardService],
    children: [
      { path: '', component: SigninComponent },
      {
        path: 'signup',
        component: SignupComponent
      }
    ]
  }
];

/** Todo modulo que vai carregar sob demanda usa forChild */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
