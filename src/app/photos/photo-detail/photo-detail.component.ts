import { UserService } from "./../../core/user/user.service";
import { AlertService } from "../../core/shared/alert/alert.service";
import { Observable } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PhotoService } from "./../photo/photo.service";
import { Photo } from "../photo/photo";
import { PhotoComment } from "../photo/photo-comment";

@Component({
  selector: "ap-photo-detail",
  templateUrl: "./photo-detail.component.html",
  styleUrls: ["./photo-detail.component.css"],
})
export class PhotoDetailComponent implements OnInit {
  photo$: Observable<Photo>;
  photoId: number;

  constructor(
    private route: ActivatedRoute,
    private photoService: PhotoService,
    private router: Router,
    private alertService: AlertService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.photoId = this.route.snapshot.params.photoId;
    this.photo$ = this.photoService.findById(this.photoId);
    this.photo$.subscribe(() => {}, err => {
      console.log(err);
      this.router.navigate(['not-found']);
  });
  }

  remove() {
    this.photoService.removePhoto(this.photoId).subscribe(() => {
      /** Emite a mensagem de foto removida e mantém a exibição até ser auto destruída. */
      this.alertService.success("Photo removed", true);
      this.router.navigate(["/user", this.userService.getUserName()]);
    });
  }
}
