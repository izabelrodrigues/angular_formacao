export interface UserForm {
  email: string;
  fullName: string;
  userName: string;
  password: string;
}
