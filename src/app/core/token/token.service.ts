import { Injectable } from '@angular/core';

const KEY = 'authToken';
@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

    /*se o valor é nulo a primeira exclamação troca pra true e a segunda pra false
    * se o valor é uma string a primeira exclamação troca para false e a segunta pra true
    */
  hasToken(){
    return !!this.getToken(); 
  }

  getToken(){
    return window.localStorage.getItem(KEY);
  }

  setToken(token: string){
    window.localStorage.setItem(KEY, token);
  }

  removeToken(){
    window.localStorage.removeItem(KEY);
  }
}
