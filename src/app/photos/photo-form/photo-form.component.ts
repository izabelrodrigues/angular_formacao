import { UserService } from "./../../core/user/user.service";
import { Router } from "@angular/router";
import { PhotoService } from "./../photo/photo.service";
import { Validators } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { AlertService } from "src/app/core/shared/alert/alert.service";

@Component({
  selector: "ap-photo-form",
  templateUrl: "./photo-form.component.html",
  styleUrls: ["./photo-form.component.css"],
})
export class PhotoFormComponent implements OnInit {
  photoForm: FormGroup;
  file: File;
  preview: string;

  constructor(
    private formBuilder: FormBuilder,
    private photoService: PhotoService,
    private router: Router,
    private alertService: AlertService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.photoForm = this.formBuilder.group({
      file: ["", Validators.required],
      description: ["", Validators.maxLength(300)],
      allowComments: [true],
    });
  }

  upload() {
    const description = this.photoForm.get("description").value;
    const allowComments = this.photoForm.get("allowComments").value;

    this.photoService
      .upload(description, allowComments, this.file)
      .subscribe(() => {
         /** Emite a mensagem de foto enviada e mantém a exibição até ser auto destruída. */
        this.alertService.success("Upload complete", true);
        this.router.navigate(["/user", this.userService.getUserName()]);
      });
  }

  handleFile(file: File) {
    this.file = file;
    this.fileAsDataURI(file);
  }

  private fileAsDataURI(file: File): void {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    /**
     * Ao completar o evendo de readAsDataUrl o result sera atribuido à variavel preview
     */
    reader.onload = (event: any) => (this.preview = event.target.result);
  }
}
