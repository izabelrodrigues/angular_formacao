import { UserForm } from './user-form';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

const API_URL = 'http://localhost:3000';

@Injectable()
export class SignupService {
  constructor(private httpClient: HttpClient) {}

  checkUserNameTaken(userName: string) {
    return this.httpClient.get(API_URL + "/user/exists/" + userName);
  }

  signUp(userForm: UserForm){
    return this.httpClient.post(API_URL + '/user/signup', userForm);
  }
}
